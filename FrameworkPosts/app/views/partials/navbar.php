<nav class="navbar navbar-expand-lg navbar-light bg-dark p-3 ">
    <a class="navbar-brand text-white" href="<?= URLROOT . '/paginas/index' ?>">MVCPosts</a>
    <a class="nav-link text-white<?= (estaActiva('index')? 'active':'')?>" href="<?= URLROOT . '/paginas/index' ?>">Home</a>
    <a class="nav-link text-white<?= (estaActiva('about')? 'active':'')?>" href="<?= URLROOT . '/paginas/about' ?>">About</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse" id="navbarTogglerDemo02">
        <ul class="ms-auto d-flex">

            <?php
            if (isLoggedIn()) {

                echo "
                    <li class='nav-item active d-flex'>
                    <p class='text-white mt-2'>Bienvenido: " . $_SESSION['user_name'] . "</p>
                    <a class='nav-link " . (estaActiva('logout') ? 'active' : '') . "text-white' href=" . URLROOT . '/users/logout' . ">Logout</a>
                    </li>";
            } else {

                echo "
                    
                    <li class='nav-item active'>
                    <a class='nav-link text-white".(estaActiva('register') ? 'active' : '') ."' href=" . URLROOT . '/users/register' . ">Register</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link text-white".(estaActiva('login') ? 'active' : '') ."' href=" . URLROOT . '/users/login' . ">Login</a>
                </li>";
            }
            ?>

        </ul>
    </div>
</nav>