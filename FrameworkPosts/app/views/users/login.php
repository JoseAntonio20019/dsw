<?php require APPROOT . '/views/partials/header.php';
require APPROOT . '/views/partials/navbar.php'; ?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">

            <h2>Iniciar sesión</h2>
            <p>Por favor, introduzca su correo y contraseña</p>
            <form method="POST" action="<?= URLROOT . '/users/login' ?>">
                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control <?php if (isset($data['email_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['email'] != '') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="Su correo electrónico" value="<?= $data['email'] ?>">
                    <?php
                    if (isset($data['email_err'])) {

                        echo '<p class="text-danger">' . $data['email_err'] . '</p>';
                    } else if ($data['email'] != '') {

                        echo '<p class="text-success">Correo electrónico correcto.</p>';
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?php if (isset($data['password_err'])) {
                                                                                    echo "is-invalid";
                                                                                }
                                                                                if ($data['password'] != '') {
                                                                                    echo 'is-valid';
                                                                                } ?>" placeholder="Su contraseña" value="<?= $data['password'] ?>">
                    <?php
                    if (isset($data['password_err'])) {

                        echo '<p class="text-danger">' . $data['password_err'] . '</p>';
                    } else if ($data['password'] != '') {

                        echo '<p class="text-success">Contraseña Correcta</p>';
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="<?= URLROOT . '/users/register' ?>">¿No tienes cuenta? Regístrate</a>
                    </div>
                    <div class="col">
                        <input type="submit" value="Iniciar sesión" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
                    <div class="flashes">

                    <?= (string) flash() ?>
                    </div>
        </div>
    </div>
</div>
<?php require APPROOT . '/views/partials/footer.php'; ?>