<?php

    require_once APPROOT . '/views/partials/header.php';
    require_once APPROOT . '/views/partials/navbar.php';

?>
<a class="btn btn-warning pull-right" href="<?= URLROOT . '/posts/index'?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5">
    <h2>Editar publicación</h2>
    <p>Por favor introduzca los datos que quiera actualizar</p>
    <form method="POST" action="<?= URLROOT . '/posts/edit/'. $data['post_id'] ?>">
        <div class="form-group">
            <label for="title">Título Nuevo: <sup>*</sup></label>
            <input type="text" name="title" class="form-control <?php if (isset($data['title_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['title_new'] != '') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="Nuevo Título" value="<?= $data['title_old'] ?>">
             <?php
                    if (isset($data['title_err'])) {

                        echo '<p class="text-danger">' . $data['title_err'] . '</p>';
                    } else if ($data['title_new'] != '' || $data['title_old']==$data['title_new'] ) {

                        echo '<p class="text-success">Título introducido correcto.</p>';
                    }
                    ?>
        </div>
        <div class="form-group">
            <label for="body">Contenido Nuevo: <sup>*</sup></label>
            <textarea name="body" class="form-control <?php if (isset($data['body_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['body_new'] != '') {
                                                                                echo 'is-valid';
                                                                            } ?>" rows="5" placeholder="Su Nuevo Contenido" value=""></textarea>
        
            <?php
                    if (isset($data['body_err'])) {

                        echo '<p class="text-danger">' . $data['body_err'] . '</p>';
                    } else if ($data['body_new'] != '') {

                        echo '<p class="text-success">Hay contenido,bien!.</p>';
                    }
                    ?>
            
        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Editar publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>

<?php

    require_once APPROOT . '/views/partials/footer.php';

?>