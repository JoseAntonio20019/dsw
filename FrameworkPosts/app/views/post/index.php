<?php

require_once APPROOT . '/views/partials/header.php';
require_once APPROOT . '/views/partials/navbar.php';


?>

<div class="row mb-3">
    <div class="col-md-6">
        <h1>Publicaciones</h1>
    </div>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="<?= URLROOT .'/posts/add'?>" role="button">
            <i class="fas fa-pencil-alt">Crear publicación</i> 
        </a>
   
</div>
<div class="flashes">

<?= (string) flash(); ?>

</div>
    


<?php 
    foreach($data as $post){

?>
<div class=card>
<div class="card-body">
    
    <h4><?= $post->title?></p></h4>
    <img src="<?= isset($post->image)? URLROOT.'/public/img/'. $post->image :'' ?>" width="200"></img>
    <p class="card-text">Creado por: <?= $post->name ?> el <?= $post->created_at?></p>
    <p class="card-text"><?= $post->body ?></p>
    <a class="btn btn-dark w-100" href="<?= URLROOT . '/posts/show/'.$post->postId?>">Más</a>
</div>
</div>
</div>

<?php
}
?>


<?php

        require_once APPROOT . '/views/partials/footer.php';

        ?>