<?php require APPROOT . '/views/partials/header.php';
      require APPROOT . '/views/partials/navbar.php';
?>
<a class="btn btn-warning pull-right" href="<?= URLROOT . '/posts/index' ?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<br>
<div class="row mb-3">
    <div class="col-md-12">
        <h1><?=$data['post']->title ?></h1>

        <div class="bg-secondary text-white p-2 mb-3">
            Creado por <?= $data['user']->name ?> el : <?= $data['user']->created_at ?>
        </div>
        <img src="<?= isset($data['post']->image)? URLROOT.'/public/img/'. $data['post']->image :'' ?>" width="200"></img>
        <p>
        
            <?= $data['post']->body; ?>
            
        </p>
        
            <hr>

            <div class="row">
                <div class="col">
            
                <?php 
                    if($_SESSION['user_id'] == $data['user']->id){
                
                    echo "
                    <a href=". URLROOT .'/posts/edit/'.$data['post']->id ." class='btn btn-success btn-block'>
                        <i class='fas fa-edit'></i> Editar
                    </a>
                </div>
                <div class='col'>
                    <form action=".URLROOT .'/posts/delete/'.$data['post']->id ." method='post'>                        
                        <button type='submit' class='btn btn-danger btn-block'>
                            <i class='fas fa-trash'></i> Borrar post
                        </button>
                    </form>
                </div>
            </div>";
                }
            ?>

            
        
    </div>
</div>

<?php require APPROOT . '/views/partials/footer.php'; ?>