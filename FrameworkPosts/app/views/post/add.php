<?php

    require_once APPROOT . '/views/partials/header.php';
    require_once APPROOT . '/views/partials/navbar.php';

?>
<a class="btn btn-warning pull-right" href="<?= URLROOT . '/posts/index'?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5">
    <h2>Crear publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
    <form enctype="multipart/form-data" method="POST" action="<?= URLROOT . '/posts/add' ?>">
        <div class="form-group">
            <label for="title">Título: <sup>*</sup></label>
            <input type="text" name="title" class="form-control <?php if (isset($data['title_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['title'] != '' || $data['title_err'] == '') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="Título de la publicación" value="<?= $data['title'] ?>">
             <?php
                    if (isset($data['title_err'])) {

                        echo '<p class="text-danger">' . $data['title_err'] . '</p>';
                    } else if ($data['title'] != '') {

                        echo '<p class="text-success">Título introducido correcto.</p>';
                    }
                    ?>
        </div>
        <div class="form-group">
            <label for="file">Añadir Imagen:</label>
        <input type="file" name="image" class="form-control <?php if (isset($data['image_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['image'] || $data['image_err']=='') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="" value="">
             <?php
                    if (isset($data['image_err'])) {

                        echo '<p class="text-danger">' . $data['image_err'] . '</p>';
                    } else if ($data['image'] != '') {

                        echo '<p class="text-success">Imagen introducida correctamente.</p>';
                    }
                    ?>

        </div>
        <div class="form-group">
            <label for="body">Contenido: <sup>*</sup></label>
            <textarea name="body" class="form-control <?php if (isset($data['body_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['body'] != '') {
                                                                                echo 'is-valid';
                                                                            } ?>" rows="5" placeholder="Su contenido"></textarea>
        
            <?php
                    if (isset($data['body_err'])) {

                        echo '<p class="text-danger">' . $data['body_err'] . '</p>';
                    } else if ($data['body'] != '') {

                        echo '<p class="text-success">Hay contenido,bien!.</p>';
                    }
                    ?>
            
        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Crear publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>

<?php

    require_once APPROOT . '/views/partials/footer.php';

?>