<?php


    class Post {


        public function __construct()
        {
            $this->db=new Database();
        }


        public function getPosts(){

             $this->db->query('SELECT *,
             posts.id as postId,
             posts.created_at as postCreatedAt,
             users.id as userId ,
             users.created_at as userCreatedAt
             FROM posts
             INNER JOIN users
             ON posts.user_id = users.id
             ORDER BY posts.created_at DESC');
            $results=$this->db->resultSet('Post');

            return $results;
        }


        public function addPost($data){

            $this->db->query("INSERT INTO posts(user_id,title,body,image) values(:user_id,:title,:body,:image);");
            $this->db->bind(':user_id',$data['user_id']);
            $this->db->bind(':title',$data['title']);
            $this->db->bind(':body',$data['body']);
            $this->db->bind(':image',$data['image']);
            $result=$this->db->execute();

            if($result){

                return true;

            }else{

                return false;
            }


        }

        public function updatePost($data){

            $this->db->query("UPDATE posts SET title=:title,body=:body where id=:id");
            $this->db->bind(':id',$data['post_id']);
            $this->db->bind(':title',$data['title_new']);
            $this->db->bind(':body',$data['body_new']);
            $updated=$this->db->execute();

            if($updated){

                return true;
            }else{

                return false;
            }

        }

        public function getPostById($id){

            $this->db->query("SELECT * from posts where id=:id");
            $this->db->bind(':id',$id);
            $post=$this->db->single('Post');

            return $post;

        }

        public function deletePost($id){

            $this->db->query("DELETE FROM posts WHERE id=:id;");
            $this->db->bind(':id',$id);
            $deleted=$this->db->execute();

            return $deleted;
        }

       

    }
