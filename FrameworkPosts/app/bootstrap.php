<?php

    if(!session_id() ) @session_start();
    
    require_once 'vendor/autoload.php';
    
    use Dotenv\Dotenv;
    $dotenv= Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    
    spl_autoload_register(function ($className){

        require_once 'libraries/'.$className .'.php';

    });

    
    


    require_once 'helpers/utils.php';
    require_once 'helpers/urlHelper.php';
    require_once 'helpers/isLoggedIn.php';

    require_once 'config/config.php';
   
    ?>