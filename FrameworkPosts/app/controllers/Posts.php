<?php

use Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller
{


    public function __construct()
    {

        if (!isLoggedIn()) {

            redirect('/users/login');
        }
        $this->userModel = $this->model("User");
        $this->postModel = $this->model("Post");
    }

    public function index()
    {
        $data = $this->postModel->getPosts();
        $this->view('post/index', $data);
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $data['title'] = trim($_POST['title'], " ");
            $data['body'] = $_POST['body'];
            $data['user_id'] = $_SESSION['user_id'];
            $data['image'] = $_FILES['image']['name'];

            filter_input_array(INPUT_POST, $data);

            if ($data['title'] == '') {

                $data['title_err'] = "Título no introducido. Introduzca uno";
            }

            if ($data['body'] == '') {

                $data['body_err'] = "Contenido no introducido. Introduzca uno";
            }

            //Añadir Image

            if (!empty($data['image'])) {

                try {
                    //Utilizamos arrTypes para indicar el tipo de archivo que admite nuestro formulario
                    $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                    $file = new File($_FILES['image'], $arrTypes);
                    $file->checker();
                    $file->saveUploadFile('img/');
                } catch (FileException $error) {

                    $data['image_err'] = $error->getMessage();
                }
            } 

            if (isset($data['title_err']) || isset($data['body_err']) || isset($data['image_err'])) {

                $this->view('post/add', $data);
            } else {

                $insertar = $this->postModel->addPost($data);


                if ($insertar) {

                    $flash = new Flash();

                    $flash->message('Post creado correctamente', 'info');

                    redirect('/posts/index');
                }
                $this->view('post/add', $data);
            };
        } else {

            $data = [

                'title' => '',
                'title_err' => '',
                'body' => '',
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'image_err' => ''
            ];

            $this->view('post/add', $data);
        }
    }




        function show($id)
        {

            $post = $this->postModel->getPostById($id);
            $user = $this->userModel->getUserById($post->user_id);

            $data['post'] = $post;
            $data['user'] = $user;


            $this->view('/post/show', $data);
        }



        function edit($id)
        {
            $post = $this->postModel->getPostById($id);
            $data['post_id'] = $post->id;
            $data['title_old'] = $post->title;


            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                $data['title_new'] = trim($_POST['title'], " ");
                $data['body_new'] = trim($_POST['body'], " ");

                filter_input_array(INPUT_POST, $data);

                if ($data['title_new'] == '') {

                    $data['title_err'] = "Título nuevo vacío. Introduzca uno";
                }

                if ($data['body_new'] == '') {

                    $data['body_err'] = "Contenido nuevo vacío. Introduzca uno";
                }

                if (isset($data['title_err']) || isset($data['body_err'])) {

                    $this->view('post/edit', $data);
                } else {

                    $updated = $this->postModel->updatePost($data);

                    if ($updated) {

                        $flash = new Flash();

                        $flash->message('Post actualizado correctamente', 'info');

                        redirect('/posts/index');
                    }
                }
            } else {

                $data = [

                    'title_new' => '',
                    'body_new' => '',

                ];
                $data['post_id'] = $post->id;
                $data['title_old'] = $post->title;


                $this->view('post/edit', $data);
            }
        }

        function delete($id)
        {

            $post = $this->postModel->getPostById($id);
            if ($_SESSION['user_id'] != $post->user_id) {

                redirect('posts/index');
            } else {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $this->postModel->deletePost($id);

                    $flash = new Flash();
                    $flash->message('Post eliminado con éxito', 'info');
                    redirect('/posts/index');
                }
            }
        }
    
}
