<?php

use Tamtamchik\SimpleFlash\Flash;

class Users extends Controller
{

           public function __construct(){


            $this->userModel= $this->model("User");   
    }
    
        
    


    public function register()
    {

        //Condicionamos si el formulario de nuestro registro es de tipo post 
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $insertado='';
            $data['name'] = trim($_POST['name']," ");
            $data['email'] = trim($_POST['email']," ");
            $data['password'] =trim($_POST['password']);
            $data['confirm_password'] = trim($_POST['confirm_password']," ");

            filter_input_array(INPUT_POST,$data);

            if ($data['name'] == '') {

                $data['name_err'] = "Introduce un nombre";
            }


            if ($data['email'] == '') {


                $data['email_err'] = "Introduce un correo electrónico";
            }

            
                $data['check']=$this->userModel->findUserbyEmail($data['email']);
                
                if($data['check']){

                $data['email_err']="El correo electrónico introducido pertenece a una cuenta existente. Introduzca otro. ";

            }    


            if ($data['password'] == '') {

                $data['password_err'] = "Introduce una contraseña";
                
            }else if(strlen($data['password'])<5){

                $data['password_err']="La contraseña es menor a 6 caracteres. Escriba una con mayor longitud.";
            }else {

               $data['password']=password_hash($_POST['password'],PASSWORD_DEFAULT);
            }

            if ($data['confirm_password'] == '') {

                $data['confirm_password_err'] = "Introduce una contraseña de confirmación";
            }

            if ($data['confirm_password'] != $_POST['password']) {

                $data['confirm_password_err'] = "Las contraseñas no coinciden, escríbelas correctamente";
            }


            if (isset($data['name_err']) || isset($data['password_err'])  || isset($data['email_err']) || isset($data['confirm_password_err'])) {

                
                $this->view('users/register',$data);
            }else{

                $insertado=$this->userModel->register($data);
            }

            
            if($insertado){

                $flash = new Flash();

                $flash->message('Ya estás registrado y puedes iniciar sesión','info');

                redirect("/users/login");
            }

            $this->view('users/register', $data);
            } else {

            //Creamos un array data  con los datos del usuario y sus respectivos
            //errores vacio y en caso de que los haya se irán añadiendo a cada campo

            $data = [
                'name' => '',

                'email' => '',

                'password' => '',

                'confirm_password' => ''

            ];

            //Cerramos el array e indicamos que en la ruta users/register haremos el formulario
            $this->view('users/register', $data);
        }
    }

   
  

    public function login()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $data['email'] = trim($_POST['email']," ");
            $data['password'] = trim($_POST['password']," ");

    
            if ($data['email'] == '') {

                $data['email_err'] = "Correo electrónico vacío. Introduzca uno";
            }

            if ($data['password'] == '') {

                $data['password_err'] = "Introduce una contraseña";

            }else if(strlen($data['password'])<5){

                $data['password_err']="La contraseña es menor a 6 caracteres. Escriba una con mayor longitud.";
            }

            if($this->userModel->findUserByEmail($data['email'])){



            }else{

                $data['email_err'] = "El usuario no se ha encontrado";
                
            }


            $user= $this->userModel->login($data['email'],$data['password']);


            if($user){

                $this->createUserSession($user);

            }else{

                $data['password_err']='Contraseña incorrecta';
                $this->view('users/login',$data);
            }
            
        } else {

            $data = [

                'email' => '',
                'password' => '',
                
                

            ];

            $this->view('users/login', $data);
        }
    }

    public function createUserSession($user){

        $_SESSION['user_id']=$user->id;
        $_SESSION['user_name']=$user->name;
        $_SESSION['user_email']=$user->email;

        redirect('/posts/index');

        

    }
    public function logout(){

        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();
        redirect('/paginas/index');
    }

  
   
   

    
}
