<?php

    define('APPROOT',dirname(dirname(__FILE__)));
    define('URLROOT', $_ENV['URLROOT']);
    define('SITENAME', $_ENV['SITENAME']);
    define('APP_VERSION',$_ENV['APP_VERSION'] );
    define('DB_HOST', $_ENV['DB_HOST']);
    define('DB_USER', $_ENV['DB_USER']);
    define('DB_PASS', $_ENV['DB_PASS']);
    define('DB_NAME', $_ENV['DB_NAME']);


?>