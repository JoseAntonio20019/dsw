<?php

//Ejercicio 1.1
//  La sentencia hace que el código se comporte en modo estricto, por lo que las funciones en PHP que no reciban parámetro tal y como se esperan darán un TypeError en vez de un warning.

declare(strict_types=1);



  $num1;
  $num2;

  function suma(int $num1,int $num2){

    return $num1 + $num2;
  }
  
  echo suma(14,10);


//Ejercicio 1.2
//Se puede especificar poniendo ':' después de especificar los parámetros



function sumaa(int $num1,int $num2):int{

    return $num1+$num2;

}

  echo sumaa(14,10);

  //Ejercicio 1.3
  // Dentro de la función si no se cumple la condición hacer que el return sea null


  //Ejercicio 2

  function insert ($nombre_tabla,$datos):string{

    $data=implode(",",array_keys($datos));
    $valor=implode(", :",array_keys($datos));
    $frase="insert into  $nombre_tabla ($data) values (:$valor')";

    return $frase;


  }

  echo insert('alumnos',['id'=>'1','nombre'=>'Jose Antonio','curso'=>'2ºDAW']);   

  //Ejercicio 3

  function insert_($nombre_tabla,$datos,&$cadena){


    $campos=implode(",",array_keys($datos));
    $valores=implode(":",array_keys($datos));

    $cadena= "insert into  $nombre_tabla ($campos) values (:$valores')";
  }
  echo '<br>';
    $cadena='insert into tabla (campos) values (valores)';
     insert_('alumnos',['id'=>'1','nombre'=>'Jose Antonio','curso'=>'2ºDAW'],$cadena);

     echo $cadena;

?>
