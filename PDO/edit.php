<?php
    session_start();
    require_once "lib/database.php";
    require_once "models/auto.php";
    $auto= new Auto();
    $auto->makeConnection();


    $id=isset($_GET['cocheid'])?$_GET['cocheid']:'';
   

    $auto1=$auto->getAuto($id);
    $mileage=$auto1->getMileage();
    $year=$auto1->getYear();
    $make=$auto1->getMake();
   
    
    
    if (isset($_POST['actualizar'])) {

        
        $marca = isset($_POST['make']) ? htmlentities($_POST['make']) : '';
        $kilometros = isset($_POST['mileage']) ? htmlentities($_POST['mileage']) : '';
        $anios =isset($_POST['year']) ?  htmlentities($_POST['year']) : '';

        $kimnum = is_numeric($kilometros);
        $yearnum = is_numeric($anios);


        if ($kilometros == '' || $year == '') {

            header("Location:edit.php?cocheid=" . $id);
            $_SESSION['mileage'] = 'Kilómetros o años no introducidos.';
            
        };
        if ($kimnum == false || $yearnum == false) {

            header("Location:edit.php?cocheid=" . $id);
            $_SESSION['mileage&year'] =  'Kilometraje y año deben ser numéricos';
            
        };

        if ($marca == '') {

            header("Location:edit.php?cocheid=" . $id);
            $_SESSION['make'] = 'El campo marca es obligatorio';
        };

            //$newAuto=new Auto($marca,$anios,$kilometros);

            //Otra opción

           /*  $auto->setMake($marca);
            $auto->setYear($anios);
            $auto->setMileage($kilometros);
            $auto->updateaAuto($auto,$id); */
            
            $_SESSION['updsuccess']="Registro actualizado correctamente";
            header("Location:autos.php");
            return;

        
        } 
      
        
        if(isset($_POST['cancelar'])){

            header("Location:autos.php");
            return;
        }

    


    require_once "views/edit.view.php";

    ?>
