<?php

session_start();
require "pdo.php";
require "models/auto.php";

$autos = new Auto();
$autos->makeConnection();

if (!isset($_SESSION['name'])) {

    die('Access Denied');
}

if($autos->rowCountAuto()==0){

    $_SESSION['sinregistro']="No se encontraron filas";
}


$error = array();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {



     if (isset($_POST['coche_id'])) {

        /* $idcoche = htmlentities($_POST['coche_id']);

        $autos->delAuto($idcoche); 


        $sql = 'DELETE FROM autos where auto_id=:id';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(':id' => $idcoche)); */
        
        


    }
        if(isset($_POST['delete'])){

            header('Location:delcheck.php?cocheid='.urlencode($_POST['cocheid'])) and die();
        
        }
        if(isset($_POST['edit'])){

            header('Location:edit.php?cocheid='.urlencode($_POST['cocheid'])) and die();
        }

        else{  

        isset($_POST['marca']) ? $marca = htmlentities($_POST['marca']) : '';
        isset($_POST['kilometros']) ? $kilometros = htmlentities($_POST['kilometros']) : '';
        isset($_POST['anios']) ? $year = htmlentities($_POST['anios']) : '';

        $kimnum = is_numeric($kilometros);
        $yearnum = is_numeric($year);


        if ($kilometros == '' || $year == '') {

            $_SESSION['mileage'] = 'Kilómetros o años no introducidos.';
        };
        if ($kimnum == false || $yearnum == false) {

            $_SESSION['mileage&year'] =  'Kilometraje y año deben ser numéricos';
        };

        if ($marca == '') {

            $_SESSION['make'] = 'El campo marca es obligatorio';
        };


        if (isset($marca) && isset($kimnum) && isset($yearnum) && $marca != '' && $kimnum != '' && $yearnum != '') {

            /*$sql = 'INSERT INTO autos (make, year,mileage) VALUES ( :mk, :yr, :mi)';
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(':mk' => $marca, ':yr' => $year, ':mi' => $kilometros));
        */
            $auto1 = new Auto($marca, $year, $kilometros);
            $autos->addAuto($auto1);
            //$error[4] = 'Registro introducido correctamente';
            $_SESSION['success']="Record Inserted";
            header("Location:autos.php");
            return;
        }
    }
}



require "views/autos.view.php";
?>