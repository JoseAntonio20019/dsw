<?php

require "partials/header.view.php";
?>

<form method="POST">

    <h3>Correo:</h3>
    <input type="text" name="email" value="">
    <br>
    <h3>Contraseña:</h3>
    <input type="password" name="password" value="">
    <input class="btn btn-primary" type="submit" value="Enviar">
    <?php if (isset($_SESSION['error'])) {
        
echo ('<p class="alert alert-danger">' . htmlentities($_SESSION['error']) . "</p>\n");
unset($_SESSION['error']);
} ?>

</form>

<?php

require "partials/footer.view.php";
?>