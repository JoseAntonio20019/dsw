<?php

require "views/partials/header.view.php";
?>

<form method="POST">

    <h3>Añadir vehículo</h3>
    <p>Marca</p>
    <input type="text" name="marca">
    <p class=" text-danger"><?php if (isset($_SESSION['make'])) {
                                echo ('<p class="alert alert-danger">' . htmlentities($_SESSION['make']) . "</p>\n");
                                unset($_SESSION['make']);
                            } ?>
    <p>Kilómetros</p>
    <input type="text" name="kilometros">
    <p class=" text-danger"><?php if (isset($_SESSION['mileage'])) {
                                echo ('<p class="alert alert-danger">' . htmlentities($_SESSION['mileage']) . "</p>\n");
                                unset($_SESSION['mileage']);
                            } else if (isset($_SESSION['mileage&year'])) {
                                echo ('<p class="alert alert-danger">' . htmlentities($_SESSION['mileage&year']) . "</p>\n");
                                unset($_SESSION['mileage&year']);
                            } ?>
    <p>Años</p>
    <input type="text" name="anios">
    <p class=" text-danger"><?php if (isset($_SESSION['mileage'])) {
                                echo ('<p class="alert alert-danger">' . htmlentities($_SESSION['mileage']) . "</p>\n");
                                unset($_SESSION['mileage']);
                            } else if (isset($_SESSION['mileage&year'])) {
                                echo ('<p class="alert alert-danger">' . htmlentities($_SESSION['mileage&year']) . "</p>\n");
                                unset($_SESSION['mileage&year']);
                            } ?>
    <p class="text-success"><?php if (isset($_SESSION['success'])) {
                                echo ('<p class="alert alert-success">' . htmlentities($_SESSION['success']) . "</p>\n");
                                unset($_SESSION['success']);
                            } else if (isset($_SESSION['delsuccess'])) {
                                echo ('<p class="alert alert-success">' . htmlentities($_SESSION['delsuccess']) . "</p>\n");
                                unset($_SESSION['delsuccess']);
                            } else if (isset($_SESSION['updsuccess'])) {
                                echo ('<p class="alert alert-success">' . htmlentities($_SESSION['updsuccess']) . "</p>\n");
                                unset($_SESSION['updsuccess']);
                            } ?></p>
    <input class="btn btn-primary" type="submit" value="Añadir">

</form>
<button class="btn btn-warning"><a class="text-decoration-none" href="logout.php">LOGOUT</a></button>

<br>
<br>
<h3>Registros:
    <?php

    echo $autos->rowCountAuto();
    ?>
</h3>


<div class="row p-1">
    <?php

    if (isset($_SESSION['sinregistro'])) {

        echo ('<p class="alert alert-warning">' . htmlentities($_SESSION['sinregistro']) . "</p>\n");
        unset($_SESSION['sinregistro']);
    }

    $arrayautos = $autos->getAutos();
    /*$stmt2 = $pdo->query("SELECT * FROM autos");*/
    foreach ($arrayautos as $row) {

    ?>

        <div class="col col-3 mt-5">
            <div class="card">

                <div class="card-body">
                    <h4>Marca</h4>
                    <p class="card-text">
                        <?= $row->getMake(); ?>
                    </p>

                    <p class="card-text">
                    <h4>Años</h4>
                    <?= $row->getYear(); ?>
                    </p>
                    <p class="card-text">
                    <h4>Kilómetros</h4>
                    <?= $row->getMileage(); ?>
                    </p>

                    <form method="POST">
                        <input type="hidden" name="cocheid" value="<?= $row->auto_id ?>">
                        <input class="btn btn-danger" type="submit" value="Delete" name="delete">
                    </form>
                    <br>
                    <form method="POST">
                        <input type="hidden" name="cocheid" value="<?= $row->auto_id ?>">
                        <input class="btn btn-warning" type="submit" value="Edit" name="edit">
                    </form>



                </div>

            </div>

        </div>


    <?php
    }
    ?>
</div>
</div>

<?php

require "partials/footer.view.php";

?>