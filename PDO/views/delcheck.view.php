<?php

    require_once "partials/header.view.php";

?>
    
        <form method="POST">
        <input type="hidden" value="<?= $id;?>" name="cocheid">
        <div class="card-body p-1 m-1 w-50">
            
        <h2>¿Quieres eliminar este coche?</h2>
        <h4>ID: <?= $id ?></h4>
        <h4>Marca: <?= $make?> </h4>
        <h4>Años: <?= $year ?> </h4>
        <h4>Kilómetros: <?= $mileage ?></h4>
        <input class="btn btn-danger" type="submit" name="eliminar" value="Eliminar">
        <input class="btn btn-primary" type="submit" name="cancelar" value="Cancelar">
        </div>
        </form>

<?php

    require_once "partials/footer.view.php";
?>