<?php


   //Creación de clase 
    class ImagenGaleria{

        private $nombre;
        private $descripcion;
        private $numVisualizaciones;
        private $numLikes;
        private $numDownloads;

        const RUTA_IMAGENES_PORTFOLIO="images/index/portfolio/";
        const RUTA_IMAGENES_GALERIA="images/index/gallery/";

        
    //Creación del constructor
    public function __construct($nombre,$descripcion,$numVisualizaciones=0,$numLikes=0,$numDownloads=0)
    {

        $this->nombre=$nombre;

        $this->descripcion=$descripcion;

        $this->numVisualizaciones=$numVisualizaciones;

        $this->numLikes=$numLikes;
        
        $this->numDownloads=$numDownloads;

    }

        //Getter & Setter

        public function getNombre(){ return $this->nombre; }
        public function setNombre($nombre): self { $this->nombre = $nombre; return $this; }

        public function getDescripcion(){ return $this->descripcion; }
        public function setDescripcion($descripcion): self { $this->descripcion = $descripcion; return $this; }

        public function getNumVisualizaciones(){ return $this->numVisualizaciones; }
        public function setNumVisualizaciones($numVisualizaciones): self { $this->numVisualizaciones = $numVisualizaciones; return $this; }

        public function getNumLikes(){ return $this->numLikes; }
        public function setNumLikes($numLikes): self { $this->numLikes = $numLikes; return $this; }

        public function getNumDownloads(){ return $this->numDownloads; }
        public function setNumDownloads($numDownloads): self { $this->numDownloads = $numDownloads; return $this; }

        public function getURLPortfolio():string{

                return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();

        }
        public function getURLGaleria():string{

            return self::RUTA_IMAGENES_GALERIA . $this->getNombre();

    }









    }
?>
