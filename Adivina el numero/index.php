
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Adivina el número</title>
</head>
<body>
<h1>Bienvenido a Adivina el Número V2</h1>
<br>  
<h3>Introduce un número</h3>
<form method="post">
<input type="text" name="numero" value="<?php echo $_POST['numero']?>"/>
<input type="submit"/>
</form>
</body>
<br>    
</html>


<?php
$numerojuego=60;
$numero= $_POST['numero'];
$numero= htmlspecialchars($numero,ENT_COMPAT,null,true);

if(trim($numero,' ') == ''){

    echo 'No has especificado ningún número';

}else if(is_numeric($numero)){

    if($numero>$numerojuego){

        echo 'El número es mayor';

    }else if($numero<$numerojuego){

        echo 'El número es menor';
    }else{

        echo 'Enhorabuena, has acertado';
    }

}else{

    echo 'Debes introducir un número';
}
?>

